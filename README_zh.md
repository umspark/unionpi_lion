# **九联Unionpi-Lion开发套件**

## 介绍
Unionpi Lion是一款基于SV823芯片，集成自研NPU、具备高性能的图像处理和编解码能力的智能硬件。芯片采用智能H.265+编码技术，可降低编码码率，有效节省硬盘空间；并集成专业安防级别的ISP，支持2~3帧宽动态融合技术和自适应降噪技术。同时搭载了NPU，提供1.0T智能算力，可支持Caffe、Tensorflow和Pytorch等主流框架，主要应用于智能安防、智慧社区、智能家居、智慧办公等场景。


图1是Unionpi-Lion开发板外观图  
![图片](/pictures/lion2%E5%8E%9F%E5%9B%BE.jpg)    

## 开发板详情  
| 规格类型 | 具体参数                                                                                                            |
| ---- | --------------------------------------------------------------------------------------------------------------- |
| 主芯片  | 亿智（EEASYTECH） SV823                                                                                                   |
| CPU  | 双核ARM Cortex-A7                                                                                    |
| NPU  | NPU@1.0TOPsGPU、支持 INT8、INT16操作                                                                                              |
| 视频处理  | H.264和H.265视频编解码的最大4M@30fp0TOPS <br>H.264BP/MP/HP，支持I/P/B帧<br>H.265MP，支持I/P/B帧<br>4K@15fps 实时JPEG解码和编码                                                         |
| 内存  | 2Gb DDR3L |
| 存储  | 双layout 支持eMMC或SPI  FLASH<br>默认使用8GB eMMC                                                                                                     |
| 以太网 | 10/100/1000M MAC +10/100M PHY                                                                                                |
| 视频输出   | 支持HDMI输出1080P30FPS<br>支持MIPI DSI屏1280*800分辨率                                  |
| 网络   | 支持802.11b/g/n 支持 2.4G WIFI，支持BT 4.2<br>百兆网口RJ45接口                                                 |
| 电源  | 12V 2A                                                                                       |
| 音频  | 支持单喇叭，单声道；最大2W，可配8欧 1W或者4欧 2W喇叭<br>支持咪头mic*1                                                              |
| Camera   | MIPI CSI 4 lane+MIPI CSI 2 lane，标配单目GC4663                                                                                                       |


图2开发板接口示意图  
![图片](/pictures/page_1.png) 




## 搭建开发环境

#### 1、安装依赖工具
安装命令如下：
```shell
sudo apt-get update && sudo apt-get install binutils git git-lfs gnupg flex
bison gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib
libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z1-dev ccache
libgl1-mesa-dev libxml2-utils xsltproc unzip m4 bc gnutls-bin python3.8
python3-pip ruby device-tree-compiler lib32stdc++6 lib32z1 libncurses5-dev -y
```

**说明**：以上安装命令适用于Ubuntu 18.04/20.04，其他版本请根据安装包名称采用对应的安装命令。

#### 2、获取标准系统源码
**前提条件**  

（1）注册码云gitee帐号。  

（2）注册码云SSH公钥，请参考[码云帮助中心](https://gitee.com/help/articles/4191)。


（3）安装[git客户端](http://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%AE%89%E8%A3%85-Git)和[git-lfs](https://gitee.com/vcs-all-in-one/git-lfs?_from=gitee_search#downloading)并配置用户信息。  


```shell
git config --global user.name "yourname"
git config --global user.email "your-email-address"
git config --global credential.helper store
```  
（4）安装码云repo工具，可以执行如下命令。  
```shell
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 
/usr/local/bin/repo  #如果没有权限，可下载至其他目录，并将其配置到环境变量中
chmod a+x /usr/local/bin/repo
pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```  
**3、获取源码操作步骤**  

（1） 通过repo + ssh 下载（需注册公钥，请参考码云帮助中心）。  

```shell
repo init -u git@gitee.com:openharmony/manifest.git -b master --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```  
（2）通过repo + https 下载。

```shell
repo init -u https://gitee.com/openharmony/manifest.git -b master --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```  
## 编译与调试  
##### 1、编译步骤  

在Linux环境进行如下操作:  

(1) hb工具下载
```shell
python3 -m pip install --user ohos-build
vim ~/.bashrc
export PATH=~/.local/bin:$PATH
source ~/.bashrc
```

(2) 选择开发板，输入如下命令：

```shell
hb set
```

(3) 移动键盘上/下键选择

```shell
unionpi_lion
```


(4) 编译，输入如下命令：

```shell
hb build -f
````

(5)检查编译结果。编译完成后，log中显示如下：

```shell
[OHOS INFO] unionpi_lion build success
[OHOS INFO] cost time: 0:01:19
````

编译完成后生成的镜像位置为：out/unionpi_lion/unionpi_lion/unionpi_lion-2022-10-20.pkg



##### 2、镜像烧录步骤  

（1）开发板与PC连接Type-C口  

（2）打开PkgBurnPro工具，选择固件  

![图片](/pictures/ruanjiantu.png)  

（3）进入烧录模式  

先后按下设备的Reset键和POL键（或者按下POL键，直至Reset键按下后松开），进入烧写状态。进入烧写状态如下图所示：  

![图片](/pictures/chushihua.png)  

当烧写进度为4%时，会有格式化操作选择窗口，选择格式化表示先格式化介质，再继续烧写，选择否则表示直接跳过格式化操作，继续进行烧写作业。格式化操作选择窗口如下：  

![图片](/pictures/geshihua.png)  

烧写成功后如下图：  

![图片](/pictures/shaoxiechenggong.png)


